const express = require("express")
const fs = require("fs")
const path = require('path')
const cors = require("cors")


const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use(express.static(path.join(__dirname, "public")));

const porta = 3001


app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`)
})

app.get("/cargainicial", function (req, resp) {

    fs.readFile("config.csv", "utf8", function (err, dados) {

        if (err) {
            console.log(err)
        } else {
            let config = []
            let informacoes = dados.split(";")

            informacoes.forEach(function (element) {
                config.push(element.split(","))
            })
            resp.send(config)
        }
    })
})

app.get("/cargainicial/:candidato", async function (req, resp) {
    var candidatos = []

    await fs.readFile("config.csv", "utf8", function (err, data) {

        var dados = data.split(";")
        dados.forEach(function (element) {
            candidatos.push(element.split(","))
        })
        for (let index = 0; index < candidatos.length; index++) {
            if (candidatos[index][2] == req.params.candidato) {
                candidatos[index][3] = __dirname + "\\public\\" + candidatos[index][3]
                resp.send(candidatos[index])
            }
        }
    })
})


app.post("/voto", function (req, resp) {

    var dataVotacao = new Date().getTime()
    let mensagem
    var votocao = req.body.rgEleitor + "," + req.body.numeroCandidato + "," + dataVotacao + ";"

    fs.appendFile("votacao.csv", votocao, function name(err) {
        if (err) {
            mensagem = {
                "status": "500",
                mensagem: "Erro ao registrar voto, contate o administrador do sistema"
            }
            resp.send(mensagem)
        } else {
            mensagem = {
                "status": "200",
                mensagem: "Voto Registrado com Sucesso"
            }
            resp.send(mensagem)
        }
    })
})

app.get("/apuracao", async function (req, resp) {

    var config = []
    await fs.readFile("config.csv", "utf8", function (err, dados) {

        if (err) {
            console.log(err)
        } else {
            let informacoes = dados.split(";")

            informacoes.forEach(function (element) {
                config.push(element.split(","))
            })
            return config
        }
    })

    await fs.readFile("votacao.csv", "utf8", function (err, data) {

        let votos = []
        let voto = data.split(";")
        let resultado = []
        var apuracao = []
        voto.forEach(function (element) {
            votos.push(element.split(","))
        })
        for (let index = 0; index < votos.length - 1; index++) {
            for (let j = 0; j < votos.length; j++) {
                if (!apuracao.includes(votos[index][1])) {
                    apuracao.push(votos[index][1])
                }
            }
        }
        apuracao.forEach(function (element) {
            resultado.push(element.split(","))
        })
        for (let index = 0; index < apuracao.length; index++) {
            resultado[index][1] = 0
            resultado[index][2] = 0
        }
        for (let index = 0; index < config.length; index++) {

            for (let j = 0; j < config.length; j++) {
                if (resultado[index][0] == config[j][1]) {
                    resultado[index][1] = config[j][2]
                    resultado[index][2] = __dirname + "\\public\\" + config[j][3]
                }
            }
        }
        for (let index = 0; index < resultado.length; index++) {
            resultado[index][3] = "0"
        }
        for (let index = 0; index < votos.length - 1; index++) {
            for (let j = 0; j < apuracao.length; j++) {
                if (apuracao[j] == votos[index][1]) {
                    resultado[j][3]++
                }
            }
        }
        resultado.sort(function (a, b) {
            return a[3] - b[3]
        });
        resp.send(resultado)
    })

})



app.get("/apuracaoidentificada", async function (req, resp) {

    var config = []
    await fs.readFile("config.csv", "utf8", function (err, dados) {

        if (err) {
            console.log(err)
        } else {
            let informacoes = dados.split(";")

            informacoes.forEach(function (element) {
                config.push(element.split(","))
            })
            return config
        }
    })

    await fs.readFile("votacao.csv", "utf8", function (err, data) {

        let votos = []
        let voto = data.split(";")
        let resultado = []
        var apuracao = []

        voto.forEach(function (element) {
            votos.push(element.split(","))
        })
        
        for (let index = 0; index < votos.length - 1; index++) {
            for (let j = 0; j < votos.length; j++) {
                if (!apuracao.includes(votos[index][1])) {
                    apuracao.push(votos[index][1])
                }
            }
        }
        
        votos = votos.filter((item, pos, array) => {
            return array.map(x => x[0]).indexOf(item[0]) === pos;
          });

          console.log(votos)
        
        // apuracao.forEach(function (element) {
        //     resultado.push(element.split(","))
        // })

        
        // for (let index = 0; index < apuracao.length; index++) {
        //     resultado[index][1] = 0
        //     resultado[index][2] = 0
        // }


        //  for (let index = 0; index < config.length -1; index++) {
        //          if (resultado[index][0] == config[index][1]) {
        //              resultado[index][1] = config[index][2]
        //              resultado[index][2] = __dirname + "\\public\\" + config[index][3]
        //          }
        //      }
         

        // for (let index = 0; index < resultado.length; index++) {
        //     resultado[index][3] = 0
        // }
        // for (let index = 0; index < votos.length; index++) {
        //     for (let j = 0; j < apuracao.length; j++) {
        //         if (apuracao[j] == votos[index][1]) {
        //             resultado[j][3]++
        //         }
        //     }
        // }
        // resultado.sort(function (a, b) {
        //     return a[3] - b[3]
        // });
        // resp.send(console.log(resultado))
    })

})




